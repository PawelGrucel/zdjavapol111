package pl.sda.service.impl;

import org.springframework.stereotype.Service;
import pl.sda.model.Order;
import pl.sda.service.OrderService;

import java.util.ArrayList;
import java.util.List;

@Service
public class InMemoryOrderService implements OrderService {

    private List<Order> orders = new ArrayList<>();

    private static int counter;

    public InMemoryOrderService() {
        orders.add(new Order(1, "napoje", 30.0));
        orders.add(new Order(2, "ksiazki", 23.6));
        counter = 2;
    }

    @Override
    public Order getById(Integer id) {
        for (Order o : orders) {
            if (o.getId().equals(id)) {
                return o;
            }
        }
        return null;
    }

    @Override
    public List<Order> getAll() {
        return orders;
    }

    @Override
    public void save(Order order) {
        order.setId(++counter);
        orders.add(order);
    }
}
