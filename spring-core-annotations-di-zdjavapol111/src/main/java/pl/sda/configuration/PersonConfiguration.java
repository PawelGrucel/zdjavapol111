package pl.sda.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.sda.dao.PersonDao;
import pl.sda.dao.impl.PersonFileDao;
import pl.sda.dao.impl.PersonMockDao;
import pl.sda.model.Person;
import pl.sda.service.PersonService;
import pl.sda.service.impl.PersonServiceImpl;
import pl.sda.validator.PersonValidator;

@Configuration
public class PersonConfiguration {

    @Bean
    public PersonDao personMockDao() {
        return new PersonMockDao();
    }

    @Bean
    public PersonDao personFileDao() {
        return new PersonFileDao();
    }

    @Bean
    public PersonValidator personValidator() {
        return new PersonValidator();
    }

    @Bean
    public PersonService personService() {
        PersonServiceImpl personService = new PersonServiceImpl();
        personService.setPersonValidator(personValidator());
        return personService;
    }
}
