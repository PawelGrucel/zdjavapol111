package pl.sda.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import pl.sda.model.User;
import pl.sda.service.AutologinService;
import pl.sda.service.UserService;

@Slf4j
@Controller
public class RegistrationController {

    @Autowired
    private UserService userService;

    @Autowired
    private AutologinService autologinService;

    @GetMapping("/registration")
    public ModelAndView showRegistrationForm() {
        ModelAndView modelAndView = new ModelAndView("registration");
        modelAndView.addObject("user", new User());
        return modelAndView;
    }

    @PostMapping("/register")
    public String handleNewUser(@ModelAttribute User user) {

        log.info("Received user: " + user);

        if (userService.existsByUsername(user.getUsername())) {
            log.info("User exists with username " + user.getUsername());
            return "registration";
        }

        userService.save(user);

        autologinService.autologin(user.getUsername());

        return "redirect:/books";

        //sprawdzić czy username już istnieje w bazie danych
        //jeśli username nie występuje w bazie, przypisz mu rolę 'USER'
        //zahashować hasło
        //zapisać obiekt do bazy
        //zaloguj użytkownika
    }

}
