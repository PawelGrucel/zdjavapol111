package pl.sda.bcrypt;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class BCryptPasswordGenerator {

    public static void main(String[] args) {

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

//        for (int i=0; i<10; i++) {
//            System.out.println(encoder.encode("alamakota"));
//        }

        final boolean result = encoder.matches("alamakota", "$2a$10$q6.8Lb9ypoug.ua8BWWubu9bG489v3QHfEoy58HtVUxsoMfmomBjq");
        System.out.println(result);
    }

}
