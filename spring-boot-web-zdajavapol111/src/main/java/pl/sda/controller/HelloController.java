package pl.sda.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Arrays;

@Slf4j
@Controller
public class HelloController {

    @GetMapping(path = "/hello")
    public String showWelcomePage() {
        log.info("Return welcome.html");
        return "welcome";
    }

    @GetMapping(path = "/hello-msg")
    public String showWelcomeMsgPage(ModelMap modelMap) {
        modelMap.addAttribute("helloMsg", "tekst z backendu");
        modelMap.addAttribute("elements", Arrays.asList("one", "two", "three"));
        return "welcome-msg";
    }

}
