package pl.sda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootWebZdajavapol111Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootWebZdajavapol111Application.class, args);
	}

}
