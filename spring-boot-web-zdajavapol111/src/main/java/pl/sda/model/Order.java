package pl.sda.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Order {

    private Integer id;

    @Size(min = 3, max = 10, message = "Size must be at least 3 and max 10")
    private String description;

    @Min(value = 0, message = "Amount must be greater than 0")
    private Double amount;

}
