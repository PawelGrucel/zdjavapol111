package pl.sda.runner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import pl.sda.component.ClassWithDependency;
import pl.sda.component.ComponentWithPropertiesCollection;
import pl.sda.component.ComponentWithValue;
import pl.sda.cyclicdependency.ClassA;
import pl.sda.primary.SimpleLogger;
import pl.sda.profile.DatabaseConnection;
import pl.sda.scope.RandomNumberReader1;
import pl.sda.scope.RandomNumberReader2;

@Component
public class ApplicationRunner implements CommandLineRunner {

    @Autowired
    private ClassWithDependency classWithDependency;

    @Autowired
    private ClassA classA;

    @Autowired
    private SimpleLogger logger;

    @Autowired
    private RandomNumberReader1 reader1;

    @Autowired
    private RandomNumberReader2 reader2;

    @Autowired
    private ComponentWithValue componentWithValue;

    @Autowired
    private ComponentWithPropertiesCollection propertiesCollection;

    @Autowired
    private DatabaseConnection databaseConnection;

    @Override
    public void run(String... args) throws Exception {
            classWithDependency.doSth();
            logger.printMessage("message from runner");

            reader1.printRandomNumber();
            reader2.printRandomNumber();

            componentWithValue.printInjectedValue();

            propertiesCollection.printUsernames();

            propertiesCollection.printMap();

            propertiesCollection.printProp();

        System.out.println(databaseConnection.getDatabaseURL());
    }
}
