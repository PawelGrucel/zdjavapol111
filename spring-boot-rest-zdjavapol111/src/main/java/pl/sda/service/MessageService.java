package pl.sda.service;

import pl.sda.model.Message;

import java.util.List;

public interface MessageService {

    Message getById(Integer id);

    List<Message> getAll();

    void add(Message message);

    void update(Message message);

    void deleteById(Integer id);

}
