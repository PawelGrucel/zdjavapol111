package pl.sda.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import pl.sda.exception.BookNotFoundException;
import pl.sda.model.Book;
import pl.sda.repository.BookRepository;
import pl.sda.service.BookService;

import java.util.List;

@Slf4j
@Primary
@Service
public class BookServiceImpl implements BookService {

    @Autowired
    private BookRepository bookRepository;

    @Override
    public void save(Book book) {
        bookRepository.save(book);
    }

    @Override
    public Book getByIsbn(String isbn) {
        Book book = bookRepository.findByIsbn(isbn);

        if (book == null) {
            throw new BookNotFoundException("Book with isbn " + isbn + " not found!");
        }
        return book;
    }

    @Override
    public List<Book> getAll() {
        return bookRepository.findAll();
    }

    @Override
    public void deleteById(Integer id) {
        bookRepository.deleteById(id);
    }

    @Override
    public void update(Book book) {
        bookRepository.save(book);
    }

    @Override
    public Book getById(Integer id) {
        return bookRepository.getById(id);
    }

    @Override
    public List<Book> getAllSortedByTitle() {
        return bookRepository.findAll(Sort.by("title").descending().and(Sort.by("author")));
    }

    @Override
    public List<Book> getAllForPageNumber(int pageNumber) {
        Page<Book> page = bookRepository.findAll(PageRequest.of(pageNumber - 1, 5, Sort.by("title")));
        log.info("Books on page: " + page.getNumberOfElements());
        log.info("Books in all pages: " + page.getTotalElements());
        log.info("All pages: " + page.getTotalPages());

        return page.getContent();
    }
}
