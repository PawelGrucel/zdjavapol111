package pl.sda.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import pl.sda.model.Order;
import pl.sda.service.OrderService;

import javax.validation.Valid;

@Slf4j
@Controller
public class OrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping(path = "/orders")
    public String showOrders(ModelMap modelMap) {
        modelMap.addAttribute("orders", orderService.getAll());
        return "order-list";
    }

    @GetMapping(path = "/create")
    public String showOrderForm(ModelMap modelMap) {
        modelMap.addAttribute("emptyOrder", new Order());
        return "order-create";
    }

    @PostMapping(path = "/save")
    public String handleNewOrder(@Valid @ModelAttribute("emptyOrder") Order order, Errors errors) {
        log.info("Handle new order: " + order);


        if (errors.hasErrors()) {
            log.error("Error occurded: " + errors.getFieldErrors());
            return "order-create";
        }

        orderService.save(order);
        return "redirect:/orders";
    }

}
