package pl.sda.service;

import pl.sda.model.Book;

import java.util.List;

public interface BookService {
    void save(Book book);

    Book getByIsbn(String isbn);

    List<Book> getAll();

    void deleteById(Integer id);

    void update(Book book);

    Book getById(Integer id);

    List<Book> getAllSortedByTitle();

    List<Book> getAllForPageNumber(int pageNumber);
}
