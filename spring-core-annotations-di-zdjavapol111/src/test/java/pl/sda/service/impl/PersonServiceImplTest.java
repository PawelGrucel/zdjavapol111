package pl.sda.service.impl;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pl.sda.configuration.PersonConfiguration;
import pl.sda.model.Person;
import pl.sda.service.PersonService;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {PersonConfiguration.class})
public class PersonServiceImplTest {

    @Autowired
    private PersonService personService;

    @Test
    public void shouldGetAllPersons() {
        //given

        //when
        final List<Person> result = personService.getAll();

        //then
        Assert.assertNotNull(result);
    }

}