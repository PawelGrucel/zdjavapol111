package pl.sda.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@Slf4j
@ControllerAdvice
public class ErrorHandlerController {

    @ExceptionHandler(Exception.class)
    public ModelAndView handleException(Exception exception) {
        log.error("Error occured: " + exception.getMessage());

        ModelAndView modelAndView = new ModelAndView("error");
        modelAndView.addObject("exceptionMessage", exception.getMessage());
        return modelAndView;
    }
}
