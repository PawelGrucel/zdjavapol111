package pl.sda.scope;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Random;

@Scope("prototype")
@Component
public class RandomNumberProvider {

    private final int value = new Random().nextInt();

    public int getValue() {
        return value;
    }

}
