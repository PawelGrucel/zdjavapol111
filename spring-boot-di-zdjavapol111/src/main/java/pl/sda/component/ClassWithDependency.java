package pl.sda.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ClassWithDependency {


    private Dependency dependency;

    public void doSth() {
        dependency.showDependency();
        System.out.println("In ClassWithDependency");
    }

    @Autowired
    public void setDependency(Dependency dependency) {
        this.dependency = dependency;
    }
}
