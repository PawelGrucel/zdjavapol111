package pl.sda.component;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ComponentWithValue {

    @Value("${pl.sda.example}")
    private String injectedValue;

    public void printInjectedValue() {
        System.out.println("Injected value from properties file: " + injectedValue);
    }

}
