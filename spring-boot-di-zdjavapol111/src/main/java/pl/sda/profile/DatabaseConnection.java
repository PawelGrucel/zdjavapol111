package pl.sda.profile;

public interface DatabaseConnection {

    String getDatabaseURL();

}
