package pl.sda.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.sda.model.Message;
import pl.sda.service.MessageService;

import java.util.List;

@Slf4j
//@Controller
//@ResponseBody
@RestController //skłąda się z dwóch powyższych
public class MessageController {

    @Autowired
    private MessageService messageService;

    //http://localhost:8080/api/messages/

    @ResponseStatus(HttpStatus.OK) //200
    @GetMapping(path = "/api/messages")
    public List<Message> messages() {
        log.info("Return all messages");
        return messageService.getAll();
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(path = "/api/messages/{id}")
    public Message messageById(@PathVariable Integer id) {
        log.info("Return message with id " + id);
        return messageService.getById(id);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(path = "/api/messages")
    public void createMessage(@RequestBody Message message) {
        log.info("Add message " + message);
        messageService.add(message);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping(path = "/api/messages")
    public void updateMessage(@RequestBody Message message) {
        log.info("Update message " + message);
        messageService.update(message);
    }

    @ResponseStatus(HttpStatus.OK)
    @DeleteMapping(path = "/api/messages/{id}")
    public void deleteMessage(@PathVariable Integer id) {
        log.info("Delete message with id " + id);
        messageService.deleteById(id);
    }

}
