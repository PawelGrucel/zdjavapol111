package pl.sda.scope;

import org.springframework.stereotype.Component;

@Component
public class RandomNumberReader1 {

    private final RandomNumberProvider randomNumberProvider;

    public RandomNumberReader1(RandomNumberProvider randomNumberProvider) {
        this.randomNumberProvider = randomNumberProvider;
    }

    public void printRandomNumber() {
        System.out.println(randomNumberProvider.getValue());
    }
}
